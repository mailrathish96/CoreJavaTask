import java.util.Scanner;

/**
 * Created by rathi-pt1424 on 1/20/2017.
 */
public class StringLength {

    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        String mystring;
        mystring = sc.next();
        int count = 0;
        for(char c : mystring.toCharArray())
        {
            count++;
        }
        System.out.println("length is: " + count);
    }
}
