import java.util.Scanner;

/**
 * Created by rathi-pt1424 on 1/20/2017.
 */
public class Fibonacci {

    public static void main(String[] args){
        int a=0,b=1;
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter the number of terms");
        int number = scanner.nextInt();
        System.out.print("0 1 ");
        for(int i=0;i<number-2;i++){
            System.out.print(a+b+" ");
            int temp = a;
            a = b;
            b = temp+b;
        }
        System.out.print("\n\n0");

        fib(0,1,number);

    }

    public static void fib(int a,int b,int n){
        if(n-2>=0){
        System.out.print(" "+b);
        fib(b,a+b,n-1);
            System.out.println(n);}
    }
}
