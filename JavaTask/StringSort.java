/**
 * Created by rathi-pt1424 on 1/20/2017.
 */
public class StringSort {
    public static void main(String[] args) {
        String Array[] = {"axa", "ba", "aza", "az", "aaa", "ade", "adf"};
        System.out.print("The array in sorted order : ");
        //sorting the array
        for (int j = 0; j < Array.length; j++) {
            for (int i = j + 1; i < Array.length; i++) {
                if (Array[i].compareTo(Array[j]) < 0) {
                    String temp = Array[j];
                    Array[j] = Array[i];
                    Array[i] = temp;
                }
            }
            System.out.print(Array[j] + " ");
        }
    }
}
