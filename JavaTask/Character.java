import java.util.Scanner;

/**
 * Created by rathi-pt1424 on 1/20/2017.
 */
public class Character {

    public static void main(String[] args){
        Scanner scanenr = new Scanner(System.in);
        String input = scanenr.nextLine();
        int lowercase= 0,uppercase= 0,special_case= 0, digit= 0;
        for(int i=0;i< input.length();i++){
            char A = input.charAt(i);
            if(A>=65 && A<=90)
                uppercase++;
            else
                if(A>=97 && A<=122)
                    lowercase++;
            else
                if(A>=48 && A<=57)
                    digit++;
            else
                special_case++;
        }
        System.out.println("Lowercase letters count - "+lowercase);
        System.out.println("Uppercase letters count - "+uppercase);
        System.out.println("digits count - "+digit);
        System.out.println("Special Characters count - "+special_case);

    }
}
