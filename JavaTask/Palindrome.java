import com.sun.deploy.util.StringUtils;

import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by rathi-pt1424 on 1/20/2017.
 */
public class Palindrome {

    public static void main(String[] args){

        System.out.println("Enter the String");
        Scanner scanner = new Scanner(System.in);
        String palindrome_string = scanner.nextLine();
        String palindrome_reverse = "";

        String s1 = null;
        String s2 = "aaaa";
        String s3 = s1+s2;
        System.out.println(s3);

        //System.out.println(Integer.toHexString(palindrome_string.hashCode()));
       // System.out.println(Integer.toHexString(palindrome_reverse.hashCode()));

        palindrome_string = palindrome_string.replaceAll("( )+"," ");
        //System.out.println("aa"+palindrome_string);
        String palindrome[] = palindrome_string.split("( )+");
        StringBuilder pal = new StringBuilder();
        pal.append(palindrome[0]);
        for( int i=1;i<palindrome.length;i++){
            pal.append(" ");
            pal.append(palindrome[i]);
        }

        palindrome_string = pal.toString();

        for(int i=palindrome_string.length()-1; i>=0 ;i--){
            palindrome_reverse = palindrome_reverse+palindrome_string.charAt(i);
        }

        if(palindrome_reverse.compareTo(palindrome_string) == 0)
            System.out.println("given string is a palindrome");
        else
            System.out.println("given string is not a palindrome");
    }
}
