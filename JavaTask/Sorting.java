/**
 * Created by rathi-pt1424 on 1/20/2017.
 */
public class Sorting {

    public static void main(String[] args){
        int array[] = {20,33,25,34,15,14,77,24,65};

        for(int i=0;i<array.length;i++)
            for(int j=0;j<array.length-1;j++){
                int temp = array[j];
                if(array[j]>array[j+1]){
                    temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                }
            }
        for(int i=0;i<array.length;i++)
            System.out.print(array[i]+" ");
    }
}
