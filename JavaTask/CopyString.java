/**
 * Created by rathi-pt1424 on 1/23/2017.
 */
public class CopyString {

    public static void main(String[] args){
        String string1 = "string1";
        String string2 = "string1";
        string1 = new String(string2);
        System.out.println(  Integer.toHexString(System.identityHashCode(string1)));
        System.out.println(  Integer.toHexString(System.identityHashCode(string2)));

    }
}
