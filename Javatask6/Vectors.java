import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

/**
 * Created by rathi-pt1424 on 1/31/2017.
 */
public class Vectors {

    public static void main(String[] args){
        Vector v = new Vector();
        v.addElement(new Integer(100));
        v.addElement(new Integer(20));
        v.addElement(new Double(10.00));
        v.addElement(new Float(40.0F));

        Enumeration enum1 = v.elements();

        while(enum1.hasMoreElements()){
            System.out.println(enum1.nextElement());
        }

        v.add(4,new Integer(1000));
        while(enum1.hasMoreElements()){
            System.out.println(enum1.nextElement());
        }
    }
}
