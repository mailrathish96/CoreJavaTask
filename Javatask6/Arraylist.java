import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by rathi-pt1424 on 1/25/2017.
 */
public class Arraylist {

    public static void main(String[] args){
        ArrayList<Base> arraylist = new ArrayList();

        Base b1 = new Base();
        Base b2 = new Base();
        b1.setName("rathish");
        b2.setName("sekar");
        arraylist.add(b1);
        arraylist.add(b2);

        for(int i=0;i<arraylist.size();i++){
            System.out.println(arraylist.get(i).getName());
        }

        Iterator iterator = arraylist.iterator();
        while (iterator.hasNext()){
            System.out.println(((Base)iterator.next()).getName());
        }
    }
}
