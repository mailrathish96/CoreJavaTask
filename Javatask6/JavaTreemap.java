import java.util.*;
public class JavaTreemap {
    public static void main(String args[]) {
        // Create and populate tree map
        Map<Integer, String> map = new TreeMap<Integer, String>();
        map.put(102, "two");
        map.put(103, "three");
        map.put(101, "one");

        for(Map.Entry<Integer, String> entry:map.entrySet()){
            System.out.println(entry.getKey()+"   "+entry.getValue());
        }
        map.remove(102);
        System.out.println("Values after remove: "+ map);
    }
} 