import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by rathi-pt1424 on 1/31/2017.
 */
public class ListSort {

    public static void main(String[] args){
        ArrayList arraylist = new ArrayList();
        arraylist.add(1);
        arraylist.add(60);
        arraylist.add(23);
        arraylist.add(45);
        arraylist.add(17);

        Collections.sort(arraylist);

        Iterator iterator = arraylist.iterator();
        while( iterator.hasNext() ){
            System.out.println(iterator.next());
        }

        System.out.println("");
        LinkedList linkedList = new LinkedList();
        linkedList.add(10);
        linkedList.add(100);
        linkedList.add(99);
        linkedList.add(34);
        linkedList.add(18);
        linkedList.add(49);

        Collections.sort(linkedList);
        Iterator iterator1 = linkedList.iterator();
        while( iterator1.hasNext() ){
            System.out.println(iterator1.next());
        }
    }
}
