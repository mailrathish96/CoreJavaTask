import java.util.Iterator;
import java.util.Properties;

/**
 * Created by rathi-pt1424 on 1/27/2017.
 */
public class Props {

    public static void main(String[] args){

        Properties properties = new Properties();
        properties.put("1", "one");
        Iterator iterator = properties.keySet().iterator();
        while (iterator.hasNext()){
            System.out.println(properties.getProperty(iterator.next().toString()));
        }
    }
}
