import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by rathi-pt1424 on 1/27/2017.
 */
public class Loggers {

    static Logger logger = Logger.getLogger(Loggers.class.getName());
    public static void main(String[] args){
//THIS
        logger.severe("message");
        //logger.severe("severe");
        logger.warning("warning");
        logger.info("information");
        logger.config("config");
        logger.fine("fine");
        logger.finer("finer");
        logger.finest("finest");
        //logger.setLevel(Level.INFO);
    }
}
