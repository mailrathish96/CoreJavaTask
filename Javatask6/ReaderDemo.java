import java.io.*;

/**
 * Created by rathi-pt1424 on 1/27/2017.
 */
public class ReaderDemo {

    public static void main(String[] args) throws IOException {
        Reader reader = new FileReader("C:\\Users\\rathi-pt1424\\Desktop\\myfile.txt");

        int data = reader.read();
        while(data != -1){
            char dataChar = (char) data;
            data = reader.read();
            System.out.print(dataChar);
        }

        Writer writer = new FileWriter("C:\\Users\\rathi-pt1424\\Desktop\\myfile.txt");

        writer.write("Hello World !!!!!");
        writer.close();
    }
}
