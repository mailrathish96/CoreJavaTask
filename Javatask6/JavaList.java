import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by rathi-pt1424 on 1/27/2017.
 */
public class JavaList {

    public static void main(String[] args){
        List list = new ArrayList();
        list.add(0,"one");
        list.add(1,"two");
        list.add(2,"three");

        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }
}
