import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by rathi-pt1424 on 1/25/2017.
 */
public class Hash {

    public static void main(String args[]) {
        Hashtable<Integer, String> hm = new Hashtable<Integer, String>();

        hm.put(101, "one");
        hm.put(102, "two");
        hm.put(103, "three");
        hm.put(104, "four");


        for (Map.Entry m : hm.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        System.out.print("\n");
        hm.remove(102);
        for (Map.Entry m : hm.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        hm.clear();
        System.out.print("\n");
        for (Map.Entry m : hm.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }

        Hashtable<Integer, Base> hm2 = new Hashtable<Integer, Base>();
        Hash hash = new Hash();
        hash.setvalue(hm2);


        for (Map.Entry m : hm2.entrySet()) {
            System.out.println(m.getKey() + " " + ((Base)m.getValue()).getName());
        }

    }

    public void setvalue(Hashtable hashtable){
        Base b1 = new Base();
        b1.setName("rathish");
        Base b2 = new Base();
        b2.setName("Sekar");
        hashtable.put(1,b1);
        hashtable.put(2,b2);
    }
    }
