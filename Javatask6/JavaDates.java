import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;

/**
 * Created by rathi-pt1424 on 1/27/2017.
 */
public class JavaDates {

    public static void main(String[] args) throws ParseException {

        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar.get(Calendar.YEAR));
        System.out.println(calendar.get(Calendar.DATE));
        System.out.println(calendar.get(Calendar.MONTH));

        Date date = new Date(117,10,10);
        DateFormat dateFormat = DateFormat.getDateInstance();
        System.out.println(dateFormat.format(date));

        date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("DD-MM-YYYY");
        System.out.println(simpleDateFormat.format(date));
        Random random = new Random();
        System.out.println(Math.random());
        System.out.println(random.nextInt(100000));
    }
}
