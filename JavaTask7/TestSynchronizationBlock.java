public class TestSynchronizationBlock{

    public static void main(String args[]){
        final Table2 obj = new Table2();//only one object

        Thread t1=new Thread(){
            public void run(){
                obj.printTable(5);
            }
        };
        Thread t2=new Thread(){
            public void run(){
                obj.printTable(100);
            }
        };

        t1.start();
        t2.start();
    }
} 