/**
 * Created by rathi-pt1424 on 1/30/2017.
 */
public class Threads {

    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<10;i++)
                System.out.println("thread one..."+i);
            }
        });
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.start();
        thread.join();

        //thread.sleep(10000);

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int j=0;j<10;j++)
                System.out.println("thread two...."+j);
            }
        });
        thread1.setPriority(Thread.MAX_PRIORITY);
        thread1.start();
        thread1.join();




    }
}
