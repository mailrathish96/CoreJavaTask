import java.util.StringTokenizer;

/**
 * Created by rathi-pt1424 on 1/19/2017.
 */
public class Tokenizer {
    Tokenizer(){
        System.out.println("Constructor");
    }
    static{
        System.out.println("this is static block");
    }

    public static void main(String[] args){

        String text = "My name is Khan";

        System.out.println("Without Delimiter");
        StringTokenizer stringTokenizer = new StringTokenizer(text);
        while(stringTokenizer.hasMoreTokens()){
            System.out.println(stringTokenizer.nextToken());
        }

        System.out.println("\nWith Delimiter");
        stringTokenizer = new StringTokenizer(text,"a");
        while(stringTokenizer.hasMoreTokens()){
            System.out.println(stringTokenizer.nextToken());
        }

        System.out.println("\nWith return Value");
        stringTokenizer = new StringTokenizer(text,"a", true);
        while(stringTokenizer.hasMoreTokens()){
            System.out.println(stringTokenizer.nextToken());
        }

        System.out.println("\nUsing Split function");
        String split[] = text.split("a");
        for(int i=0;i<split.length;i++){
            System.out.println(split[i]);
        }
    }
}
