import java.util.Scanner;

/**
 * Created by rathi-pt1424 on 1/23/2017.
 */
public class RemoveSubstring {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the sentence ");
        String sentence = scanner.nextLine();

        System.out.print("Enter the substring to be removed ");
        String substring = scanner.nextLine();
        sentence = sentence.replaceAll(substring,"");
        System.out.println(sentence);
    }
}
