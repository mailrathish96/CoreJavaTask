import Task4.Outer_Demo;
import Task4.ParentClass;
import Task4.Outer_Demo.Inner_Demo3;
public class Package {

    public static void main(String[] args){
        ParentClass parentClass = new ParentClass();
        parentClass.print("Package class..");
        Outer_Demo outer = new Outer_Demo();
        Inner_Demo3 inner_demo3 = outer.new Inner_Demo3();
        inner_demo3.print();
    }
}
