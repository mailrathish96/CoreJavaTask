package Task4;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.InputMismatchException;
import java.util.Scanner;


public class ExceptionHandling {

    public static void main(String[] args){

        System.out.println(ret());
        try{
            /*deposit(1000);
            if(100>10)
                throw new Exception();*/
            Scanner scanner = new Scanner(System.in);
            char A = scanner.next(".").charAt(0);
            FileReader fileReader = new FileReader("rathish.txt");
            fileReader.read();

        }catch (InputMismatchException IMe){
            System.out.println("Input mismatch Exception"+IMe.fillInStackTrace());
            //Ime.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println("file not found exception");
            //e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO exception");
            //e.printStackTrace();
        } catch (Exception Re){
            System.out.println("Exception");
        }
        finally {
            System.out.println("This block executes all time becoz it is in finally block");
        }



    }

    public static void deposit(double amount) throws RemoteException {
        if(amount > 100)
        throw new RemoteException();
    }

    public static int ret(){
        int x=0;
        try{
            if(x==1)
            throw new Exception();
            //return 1;
        }
        catch (Exception e){
            return 2;
        }
        finally{
            //return 3;
        }
        return 4;
    }

}
