package Task4;

public class Outer_Demo {
    int num;

    private class Inner_Demo {
        public void print() {
            System.out.println("This is an inner class with private");
        }
    }
    protected class Inner_Demo2 {
        public void print() {
            System.out.println("This is an inner class with protected");
        }
    }
    public class Inner_Demo3 {
        public void print() {
            System.out.println("This is an inner class with public");
        }
    }

    void display_Inner() {
        Inner_Demo inner = new Inner_Demo();
        inner.print();
    }
}