/**
 * Created by rathi-pt1424 on 1/24/2017.
 */
public class Equals {

    public static void main(String[] args){
        String string1 = "string";
        String string2 = "string";
        System.out.println(string1==string2);
        System.out.println(string1.equals(string2));
        String string3 = new String("string");
        String string4 = new String("string");
        System.out.println(string3 == string4);
        System.out.println(string3.equals(string4));
    }
}
