/**
 * Created by rathi-pt1424 on 1/24/2017.
 */
public class Scope {

    static int x=100;
    public static void main(String[] args){
        int x=10;
        {int y=10;}
        System.out.println("Inside main "+x);
        method();
    }
    public static void method(){
        int x=1000;
        System.out.println("Inside method "+ x);
    }
}
