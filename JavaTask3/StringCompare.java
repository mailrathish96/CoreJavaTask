/**
 * Created by rathi-pt1424 on 1/23/2017.
 */
public class StringCompare {

    public static void main(String[] args){
        String string1 = "string";
        String string2 = "STRING";

        if(string1.compareTo(string2)!=0)
            System.out.println("string comparison with case sensitivity");
        if(string1.compareToIgnoreCase(string2) == 0)
            System.out.println("String comparison without case sensitivity");
    }
}
