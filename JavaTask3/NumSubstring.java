/**
 * Created by rathi-pt1424 on 1/23/2017.
 */
public class NumSubstring {

    public static void main(String[] args){

        String substring = "str";
        int length = substring.length();
        String sentence = "This is a string with substring str.";
        int count=0;
        for(int i=0;i<sentence.length()-length;i++){
            if(sentence.substring(i,i+length).compareTo(substring) == 0)
                count++;
        }
        System.out.println("number of occurences of substring in string is "+ count);
    }
}
