import java.util.Scanner;

/**
 * Created by rathi-pt1424 on 1/24/2017.
 */
public class Split {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the string ");
        String input = scanner.nextLine();
        System.out.print("Enter the delimiter ");
        String delimiter = scanner.nextLine();

        String[] array = input.split(delimiter);
        for(int i=0;i<array.length;i++){
            System.out.println(array[i]);
        }

        //rathish is from trichy.
    }
}
