import sun.util.resources.cldr.aa.CalendarData_aa_DJ;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class DateandTime {

    public static void main(String[] args) throws ParseException {

        DateFormat df = new SimpleDateFormat("a EEEE d MMM YYY HH:mm:ss.sssssXXX");
        Date dateobj = new Date();
        System.out.println(df.format(dateobj));

        Calendar calobj = Calendar.getInstance();
        System.out.println(df.format(calobj.getTime()));


        /*Date date1 = new Date("30/05/1996");
        Date date2 = new Date("19/05/1996");
        System.out.println(date1.compareTo(date2));

        if(date1.equals(date2))
            System.out.println("both dates are same....");
        else
            if(date1.before(date2))
                System.out.println("date1 is before");
            else
                if(date1.after(date2))
                    System.out.println("date1 is after date2");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date1);
        System.out.println(calendar.getTimeInMillis());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        System.out.println(calendar.getTimeInMillis());
        System.out.println(System.currentTimeMillis());*/

        DateFormat d = new SimpleDateFormat("dd/MM/YYYY hh-mm-ss");
        Date dtd = new Date();
        Scanner scanner = new Scanner(System.in);
        Date date = new Date();
        System.out.println("Enter day month year hours mins sec");
        int day = scanner.nextInt();
       // date.
              date.setDate(day);
        int month = scanner.nextInt();
        date.setMonth(month);
        int year = scanner.nextInt();
        date.setYear(year-1900);
        int hours = scanner.nextInt();
        date.setHours(hours);
        int mins = scanner.nextInt();
        date.setMinutes(mins);
        int sec = scanner.nextInt();
        date.setSeconds(sec);

        if(date.equals(dtd))
            System.out.println("both dates are same....");
        else
            if(date.before(dtd))
                System.out.println("input is before current");
            else
                if(date.after(dtd))
                    System.out.println("input is after current");
    }
}
