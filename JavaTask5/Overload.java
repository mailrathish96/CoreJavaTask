import java.util.ArrayList;

/**
 * Created by rathi-pt1424 on 1/24/2017.
 */
public class Overload {

    public static void main(String[] args){

        display(10);
        display(10,20);
        display("Integer");
        display("Integer", 10);
    }
    public static void display(int x){
        System.out.println("Integer "+x);
    }
    public static void display(int x, int y){
        System.out.println("Integers.. "+x+" "+y);
    }
    private static void display(String string){
        System.out.println(string);
    }
    public void display(){
        System.out.println("display called");
    }
    protected static void display(String string,int x){
        System.out.println(string+" "+x+" Protected");
    }
}