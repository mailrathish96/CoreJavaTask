import java.lang.*;
import java.lang.Override;


public class ClassA extends DataHiding implements MyInterface{
    @java.lang.Override
    void display() {
        System.out.println("Display......");
    }

    public static void main(String[] args){
        ClassA classA = new ClassA();
        classA.method();
        MyInterface mf = new ClassA();
        mf.out();
        MyInterface.print();
    }

    @Override
    public void method() {
        System.out.println("Method of MyInterface and variable "+x);
    }
}
