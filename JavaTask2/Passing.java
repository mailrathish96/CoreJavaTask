/**
 * Created by rathi-pt1424 on 1/23/2017.
 */
public class Passing {

    int value =1000;
    Passing(int value){
        this.value = value;
    }
    public void display(Passing x){
        x.value=10;
        System.out.println("\n\nvalue changed to "+x.value);
    }

    public static void Strings (String string){
        string = "sekar";
        System.out.println(string);
    }

    public static void Int (int x){
        x = 10;
        System.out.println(x);
    }

    public static void Arrays (int[] array){
        for(int i=0;i<array.length;i++){
            array[i] = i;
            System.out.print(array[i]+" ");
        }
        System.out.print("\n");
    }

    public static void String_Arrays (String[] array){
        for(int i=0;i<array.length;i++){
            array[i] = ""+i;
            System.out.print(array[i]+" ");
        }
        System.out.print("\n");
    }

    public static void swap(Call c, Call c1){
        int temp = c.val;
        c.val = c1.val;
        c1.val = temp;
    }
    public static void main(String[] args){

        int x=0;
        System.out.println("Int-value before passing by value "+x);
        Int(x);
        System.out.println("Int-value after passing by value "+x+"\n");

        String string = "rathish";
        System.out.println("String-value before passing by value "+string);
        Strings(string);
        System.out.println("String-value after passing by value "+string+"\n");

        int array[] ={10,20,30,40,50,60};
        for(int i=0;i<array.length;i++){
            System.out.print(array[i]+" ");
        }
        System.out.print("\n");
        Arrays(array);
        for(int i=0;i<array.length;i++){
            System.out.print(array[i]+" ");
        }

        String string_array[] ={"red","blue","green","yellow","violet","white"};
        System.out.println("\n");
        for(int i=0;i<string_array.length;i++){
            System.out.print(string_array[i]+" ");
        }
        System.out.print("\n");
        String_Arrays(string_array);
        for(int i=0;i<array.length;i++){
            System.out.print(string_array[i]+" ");
        }

        System.out.println("\n\n");
        Call call1 = new Call();
        call1.val = 100;
        Call call2 = new Call();
        call2.val = 1000;

        System.out.println(call1.val+" "+call2.val);
        swap(call1,call2);
        System.out.println(call1.val+" "+call2.val);
    }
}
