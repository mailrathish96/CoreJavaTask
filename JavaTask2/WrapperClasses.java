import jdk.internal.org.objectweb.asm.tree.analysis.Value;

import java.util.DoubleSummaryStatistics;

/**
 * Created by rathi-pt1424 on 1/23/2017.
 */
public class WrapperClasses {

    public static void main(String[] args){

        int a=10;
        String x="23";
        double d = 0.04;
        float f = 0.2F;
        Float F = new Float(f);
        Double D = new Double(d);

        Integer float_to_int = F.intValue();

        Integer x1 = new Integer(10);
        int pp = x1.intValue();

        Integer double_to_int = D.intValue();
        Integer string_to_int = new Integer(x);
        Integer i = new Integer(23);
        Double int_to_double = i.doubleValue();
        Float int_to_float = i.floatValue();
        Double float_to_double  = F.doubleValue();
        Float double_to_flaot = D.floatValue();


        System.out.println(string_to_int);
        System.out.println("float to int "+float_to_int);
        System.out.println("double to int "+double_to_int);
        System.out.println("string to int "+string_to_int);
        System.out.println("int to double "+double_to_int);
        System.out.println("int to flaot "+float_to_int);
        System.out.println("flaot to double "+float_to_double);
        System.out.println("double to float "+double_to_flaot);

    }
}
