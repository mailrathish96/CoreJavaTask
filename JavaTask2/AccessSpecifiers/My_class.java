package AccessSpecifiers;


public class My_class extends Outer_Demo {

    public static void main(String args[]) {
        Outer_Demo outer = new Outer_Demo();
        Outer_Demo.Inner_Demo2 inner = outer.new Inner_Demo2();
        inner.print();
        Outer_Demo.Inner_Demo3 inner3 = outer.new Inner_Demo3();
        inner3.print();
    }
}
