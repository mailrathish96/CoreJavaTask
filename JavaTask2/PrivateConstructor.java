/**
 * Created by rathi-pt1424 on 1/23/2017.
 */
public class PrivateConstructor {
    static {
        System.out.println("block");
    }

    private PrivateConstructor(){
        System.out.println("this is Private constructor");
    }

   /* public static void main(String[] args){

        PrivateConstructor privateConstructor = new PrivateConstructor();
    }*/

    public void test(){
        System.out.println("function calling private constructor without main method");
    }
    public static PrivateConstructor function(){
        PrivateConstructor privateConstructor = new PrivateConstructor();
        return privateConstructor;
    }
}
