import java.util.Enumeration;
import java.util.Properties;

/**
 * Created by rathi-pt1424 on 1/23/2017.
 */
public class SystemProperties {

    public static void main(String[] args){

        Properties p = System.getProperties();

        System.out.println("Working Directory " + System.getProperty("user.dir")+"\n\n");


        Enumeration keys = p.keys();
        while (keys.hasMoreElements()) {
            String key = (String)keys.nextElement();
            String value = (String)p.get(key);
            System.out.println(key + "  : " + value);
        }

    }
}
